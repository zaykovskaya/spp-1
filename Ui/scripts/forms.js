(() => {
  const filters = document.querySelectorAll(".filter");
  filters.forEach(filter => {
    const checkBoxState = sessionStorage.getItem(filter.id);

    filter.checked = checkBoxState
      ? JSON.parse(sessionStorage.getItem(filter.id))
      : false;
  });
  console.log(filters);
})();

document.querySelector(".filter-form").addEventListener("click", e => {
  const target = e.target;
  if (target.tagName === "INPUT") {
    sessionStorage.setItem(
      target.id,
      document.querySelector(`#${target.id}`).checked.toString()
    );
  }
});

document.querySelector(".task-form").addEventListener("submit", e => {
  e.preventDefault();
  postTask();
});

document.querySelector(".filter-form").addEventListener("submit", e => {
  e.preventDefault();
  applyFilter();
});

const getFileNames = () => {
  const fileNames = [];
  const file = [...document.getElementById("file").files];
  file.forEach(element => {
    fileNames.push(element.name);
  });
  console.log(file);
  return fileNames;
};

const getFilters = formData => {
  const filters = [];
  for (let pair of formData.entries()) {
    filters.push(pair[1]);
  }
  return filters;
};

const getFormData = selector => {
  const form = document.querySelector(selector);
  const formData = new FormData(form);

  return formData;
};

const postTask = () => {
  const formData = getFormData(".task-form");

  const fileNames = getFileNames();
  formData.append("fileNames", fileNames);

  const filters = getFilters(getFormData(".filter-form"));
  formData.append("filters", filters);

  const requestOptions = {
    method: "POST",
    body: formData
  };

  fetch("/postTask", requestOptions)
    .then(el => el.json())
    .then(res => {
      location.href = "/";
    });
};

const applyFilter = () => {
  const formData = getFormData(".filter-form");
  const filters = getFilters(formData);

  const requestOptions = {
    method: "POST",
    headers: {
      "Content-Type": "application/json"
    },
    body: JSON.stringify({ filters })
  };

  fetch("/filterByStatus", requestOptions)
    .then(el => el.json())
    .then(res => {
      location.href = "/";
    });
};
