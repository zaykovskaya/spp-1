const formidable = require("formidable");
const fs = require("fs-extra");
const path = require("path");
const moment = require("moment");

const tasks = require("../data/tasks");
let mutableTasks = JSON.parse(JSON.stringify(tasks));

const getTasks = (req, res) => {
  res.render(path.join(__dirname, "../../Ui/views/pages/tasks.ejs"), {
    time: moment().format("YYYY-MM-D"),
    tasks: mutableTasks
  });
};

const postTask = (req, res) => {
  const form = new formidable.IncomingForm();
  form.parse(req, function(err, fields, files) {
    const { filters, fileNames, ...newTask } = fields;

    newTask.files = fields.fileNames.split(",") || [];

    const task = tasks.find(task => task.summary === fields.summary);
    if (task) {
      tasks[tasks.indexOf(task)] = newTask;
    } else {
      tasks.push(newTask);
    }

    mutableTasks = filters.length
      ? tasks.filter(task => filters.includes(task.status))
      : JSON.parse(JSON.stringify(tasks));

    res.status(200).send({});
  });

  form.on("end", function(fields, files) {
    this.openedFiles.forEach(file => {
      const temp_path = file.path;
      const file_name = file.name;
      const new_location = "./../Ui/files/";

      fs.copy(temp_path, new_location + file_name, function(err) {
        if (err) {
          console.error(err);
        } else {
          console.log("File saved!");
        }
      });
    });
  });
};

const filterTasksByStatus = (req, res) => {
  const { filters } = req.body;
  mutableTasks = tasks.filter(task => filters.includes(task.status));
  res.status(200).send({});
};

module.exports = {
  getTasks,
  postTask,
  filterTasksByStatus
};
