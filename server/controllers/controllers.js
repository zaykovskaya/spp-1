const services = require("../services/services");

const postTask = (req, res) => {
  services.postTask(req, res);
};

const getTasks = (req, res) => {
  services.getTasks(req, res);
};

const filterTasksByStatus = (req, res) => {
  services.filterTasksByStatus(req, res);
};

module.exports = {
  postTask,
  getTasks,
  filterTasksByStatus
};
