const express = require("express");
const app = express();
const path = require("path");
const bodyParser = require("body-parser");
const PORT = 3000;
const controllers = require("./controllers/controllers");

app.set("view engine", "ejs");
app.use(express.static(path.join(__dirname, "../Ui")));

app.use(
  bodyParser.urlencoded({
    extended: false
  })
);

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.get("/", (req, res) => {
  controllers.getTasks(req, res);
});

app.post("/postTask", (req, res) => {
  controllers.postTask(req, res);
});

app.post("/filterByStatus", (req, res) => {
  controllers.filterTasksByStatus(req, res);
});

app.listen(PORT, () => {
  console.log("\x1b[36m%s\x1b[0m", `Server is running on ${PORT} port`);
});
